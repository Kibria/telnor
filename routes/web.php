<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    return view('welcome');
//    $app = Redis::connection();
//    $app->set("key2","value2")
//    print_r(app()->make('redis'));
    $key = "countries";
    $redis = Redis::connection();
    $redis->expire($key, 300);
    $redis->sadd($key, 'china');
    $redis->sadd($key, ['england', 'france', 'germany']);
    $redis->sadd($key, 'china'); // this entry is ignored

    $redis->srem($key, ['england', 'china']);

    $redis->sismember($key, 'england'); // false

    $redis->smembers($key); // ['france', 'germany']
    $redis->ttl($key);
    return $redis->smembers($key);
});

//Route::get('/', 'AppoinmentController@index');